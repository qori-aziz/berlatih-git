<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account Baru!</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method = "POST">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="namadepan"> <br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="namabelakang"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="nationality">
            <option value="indonesia"> Indonesia </option>
            <option value="malaysia"> Malaysia </option>
            <option value="other"> Other </option> 
        </select> <br> <br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="bind"> Bahasa Indonesia <br>
        <input type="checkbox" name="bing"> English <br>
        <input type="checkbox" name="blain"> Other <br><br>

        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value ="Sign Up">

    </form>
</body>
</html>