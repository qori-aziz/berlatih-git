<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function form(){
        return view('form');
    }

    public function form_post(Request $request){
        $namadepan = $request["namadepan"];
        $namabelakang = $request["namabelakang"];
        $fullname = $namadepan . " " .$namabelakang;
        return view('welcome1', compact('namadepan','namabelakang'));
    }

    public function form_get(){
        return view('welcome1', ['namadepan' => "", 'namabelakang' => ""]);
    }
}
