<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    //
    public function index()
    {
      $casts = DB::table('casts')->get();
      //dd($casts);
      return view('post.index', compact('casts'));
    }

    public function create(){
        return view('post.create');
    }

    public function store(Request $request){
        $query = DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast')->with('success', 'Cast berhasil disimpan');
    }

    public function show($id){
        $casts = DB::table('casts')->where('id',$id)->first();
        return view('post.show', compact('casts'));
    }

    public function edit($id){
        $casts = DB::table('casts')->where('id',$id)->first();
        return view('post.edit', compact('casts'));
    }

    public function update($id, Request $request){
        $query = DB::table('casts')
        ->where('id', $id)
        ->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio']
        ]);
        return redirect('/cast')->with('success', 'Berhasil Update');
    }

    public function destroy($id){
        DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Cast Berhasil Dihapus');
    }
}
